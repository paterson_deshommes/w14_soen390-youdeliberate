﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using YouDeliberate.Controllers;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.Services;
using System.Web.Mvc;
using Moq;
using YouDeliberate.Models.DAO;
using System.Web;
using System.Web.Routing;
using System.Collections.Specialized;
using System.IO;

namespace YouDeliberate.Tests.Controllers
{
    [TestClass]
    public class AdminControllerTest
    {
        private AdminController ac;
        private Mock<INarrativeService> dataService;
        private Mock<IAdminService> adminService;
        private Mock<IAuthService> authService;
        private Mock<IUploadService> uploadService;
        private Mock<IFileService> fileService;
        private Mock<HttpSessionStateBase> session;
        private Mock<HttpContextBase> httpContext;
        private readonly string ADMIN = "admin";

        public AdminControllerTest()
        {
            dataService = new Mock<INarrativeService>();
            authService = new Mock<IAuthService>();
            uploadService = new Mock<IUploadService>();
            fileService = new Mock<IFileService>();
            adminService = new Mock<IAdminService>();

            httpContext = new Mock<HttpContextBase>();
            // mock your session 
            session = new Mock<HttpSessionStateBase>();
            // get property, so when session will get return value when access at the controller 
            session.SetupGet(x => x["admin"]).Returns("admin");
            // set property, so's able to be set on controller 
            session.SetupSet(x => x["admin"] = It.IsAny<string>())
            .Callback((string key, object value) =>
            {
                session.SetupGet(x => x["admin"]).Returns(value);
            });
            httpContext.Setup(x => x.Session).Returns(session.Object);

            RequestContext requestContext = new RequestContext(httpContext.Object, new RouteData());

            ac = new AdminController(uploadService.Object, fileService.Object, dataService.Object, authService.Object)
            {
                Url = new UrlHelper(requestContext),
                ControllerContext = new ControllerContext()
                {
                    RequestContext = requestContext
                }
            };
        }

        [TestMethod]
        public void TestIndex()
        {
            ActionResult result;

            result = ac.Index();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestConfig()
        {
            administrator admin = new administrator();
            Mock<IAdminDAO> iadao = new Mock<IAdminDAO>();

            admin.adminID = 1;
            admin.username = "haha";
            admin.password = "pass";
            admin.email = "email";

            AuthService auth = new AuthService();

            List<administrator> adminList = new List<administrator> {admin};
            adminService.Setup(ds => ds.GetAllAdmins()).Returns(adminList);

            ActionResult result;
            result = ac.Config();
            Assert.AreEqual("Config", auth.VerifyLogin("Config", session.Object));
            Assert.IsNotNull(result);
        }


        [TestMethod]
        public void TestUpload()
        {
            ViewResult result = ac.Upload() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void TestMetrics()
        {
            ViewResult result = ac.Metrics() as ViewResult;
            Assert.AreEqual("", result.ViewName);
        }

        [TestMethod]
        public void TestLoginFail()
        {
            var result = ac.Login(new FormCollection()) as RedirectToRouteResult;
            Assert.IsNull(result);
        }

        [TestMethod]
        public void TestGetFlagsFalse()
        {
            var result = ac.GetFlags(-1) as RedirectToRouteResult;
            Assert.IsNull(result);
        }

        [TestMethod]
        public void TestEmptySetViewable()
        {
            var result = ac.SetViewable(new FormCollection()) as RedirectToRouteResult;
            Assert.IsNull(result);
        }

        [TestMethod]
        public void TestEmptyUpload()
        {
            FormCollection form = new FormCollection();
            RedirectToRouteResult result = null;
            var postedfile = new Mock<HttpPostedFileBase>();
            postedfile.Setup(f => f.ContentLength).Returns(8192);
            postedfile.Setup(f => f.FileName).Returns("myfile.txt");
            postedfile.Setup(f => f.InputStream).Returns(new MemoryStream(8192));

            try
            {
                result = ac.Upload(postedfile.Object, form) as RedirectToRouteResult;
            }catch(NullReferenceException)
            {
                Assert.IsNull(result);
            }
        }

        [TestMethod]
        public void TestListFailLogin()
        {
            var result = ac.List("a",0,0) as RedirectToRouteResult;
            Assert.IsNull(result);
        }

        [TestMethod]
        public void TestEmptySetCategory()
        {
            var result = ac.SetCategory(new FormCollection()) as RedirectToRouteResult;
            Assert.IsNull(result);
        }
        

        [TestMethod]
        public void TestLogout()
        {
            ActionResult result;
            AuthService auth = new AuthService();
            result = ac.Logout();
            Assert.AreNotEqual("Index", auth.VerifyLogin("Config", session.Object));
        }


    }
}
﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using YouDeliberate.Controllers;
using YouDeliberate.Models.DAO;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.Services;

namespace YouDeliberate.Tests.Controllers
{
    
    [TestClass]
    public class BrowseControllerTest
    {
        private BrowseController bc;
        private Mock<INarrativeService> dataService;

        public BrowseControllerTest()
        {
            dataService = new Mock<INarrativeService>();
            bc = new BrowseController(dataService.Object);
        }

        [TestMethod]
        public void TestGetPlaylist()
        {
            dataService.ResetCalls();

            List<Media> media = new List<Media>{
                new Media (
                    new audio { path = "TEST.mp3" },
                    new picture { path = "TEST.jpg" }
                ),
                new Media (
                    new audio { path = "TEST2.mp3" },
                    new picture { path = "TEST2.jpg" }
                )
            };
            dataService.Setup(ds => ds.GetPlayList(It.IsAny<int>())).Returns(media);

            JsonResult jsonResult = bc.GetPlaylist(800) as JsonResult;
            List<string> result = jsonResult.Data as List<string>;

            List<string> expected = new List<string> { "TEST.mp3", "TEST.jpg", "TEST2.mp3", "TEST2.jpg" };
            Assert.IsTrue(result.SequenceEqual(expected));
        }

        [TestMethod]
        public void TestGetPlaylistNull()
        {
            dataService.ResetCalls();

            dataService.Setup(ds => ds.GetPlayList(It.IsAny<int>())).Returns(() => null);

            HttpStatusCodeResult result = bc.GetPlaylist(-1) as HttpStatusCodeResult;

            Assert.AreEqual(550, result.StatusCode);
        }

        [TestMethod]
        public void TestSortViewNarratives()
        {
            dataService.ResetCalls();

            List<narrative> narrativeList = new List<narrative>();
            narrativeList.Add(new narrative { numberViews = 102 });
            narrativeList.Add(new narrative { numberViews = 80 });
            narrativeList.Add(new narrative { numberViews = 300 });

            dataService.Setup(ds => ds.GetNarratives(
                It.IsAny<int>(), 
                It.IsAny<int>(), 
                It.IsAny<bool>(), 
                It.IsAny<bool>(), 
                It.IsAny<bool>()))
                .Returns(narrativeList);

            dataService.Setup(ds => ds.SortNarratives(narrativeList, "MostViews"))
                .Returns(new NarrativeService().SortNarratives(narrativeList, "MostViews"));

            ViewResult result = bc.SortViewNarratives("MostViews", "TEST", "TEST") as ViewResult;
            List<narrative> resultList = result.Model as List<narrative>;

            narrativeList = narrativeList.OrderByDescending(n => n.numberViews).ToList();
            Assert.IsTrue(narrativeList.SequenceEqual(resultList));
        }
    }
}
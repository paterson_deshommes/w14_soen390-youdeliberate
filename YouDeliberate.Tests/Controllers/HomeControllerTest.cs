﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using YouDeliberate.Controllers;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.Services;
using System.Web.Mvc;
using Moq;
using YouDeliberate.Models.DAO;
using System.Web;
using System.Web.Routing;
using System.Collections.Specialized;
using System.IO;


namespace YouDeliberate.Tests.Controllers
{

    [TestClass]
    public class HomeControllerTest
    {
        private HomeController hc;
        private Mock<ITopicService> topicService;

        public HomeControllerTest()
        {
            topicService = new Mock<ITopicService>();
            hc = new HomeController();
        }

        [TestMethod]
        public void TestIndex()
        {
            ActionResult result;

            result = hc.Index();
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void TestGetTopic()
        {
            hc = new HomeController();
            ActionResult result = hc.GetTopic();
            Assert.IsNotNull(result);
        }
    }
}

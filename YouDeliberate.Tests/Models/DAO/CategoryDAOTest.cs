﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using YouDeliberate.Models.DAO;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.Services;

namespace YouDeliberate.Tests.Models
{
    [TestClass]
    public class CategoryDAOTest
    {
        private NarrativeService dataService = new NarrativeService();

        [TestMethod]
        public void retrieve_Category_By_ID_Test()
        {
            CategoryDAO dao = new CategoryDAO();
            category expected = new category();
            expected.category1 = "Agree";
            category actual = dao.retrieveCategoryByID(1);
            Assert.AreEqual(expected.category1, actual.category1, true);
        }

        /*[TestMethod]
        public void TestGetCategoryByName()
        {
            category expected = new category();
            expected.categoryID = 1;
            category actual = dataService.GetCategory("Agree");
            Assert.AreEqual<int>(expected.categoryID, actual.categoryID);
        }*/

        [TestMethod]
        public void retrieve_Non_Existant_Category_By_ID_Test()
        {
            CategoryDAO dao = new CategoryDAO();
            category actual = dao.retrieveCategoryByID(-1);
            Assert.IsNull(actual);
        }
    }
}

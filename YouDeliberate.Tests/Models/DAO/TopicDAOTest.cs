﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.DAO;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace YouDeliberate.Tests.Models.DAO
{
    [TestClass]
    public class TopicDAOTest
    {
        [TestMethod]
        public void TestInsertTopic()
        {
            TopicDAO topDAO = new TopicDAO();
            topic tp = new topic();
            tp.content = "test";
            tp.subcontent = "test";
            tp.inUse = false;
            topic expected = topDAO.InsertTopic(tp);
            int topID = expected.topicID;
            topic actual = new topic();
            actual = topDAO.GetTopic(topID);
            Assert.AreEqual(expected.topicID, actual.topicID);
            Assert.AreEqual(expected.content, actual.content);
            Assert.AreEqual(expected.subcontent, actual.subcontent);

            using (var db = new soen390w14teamdEntities())
            {
                db.Database.ExecuteSqlCommand("DELETE FROM topics WHERE topicId={0}", topID);
            }

        }

        [TestMethod]
        public void TestInsertNullTopic()
        {
            TopicDAO topDAO = new TopicDAO();
            try
            {
                topDAO.InsertTopic(null);
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual("Value cannot be null.\r\nParameter name: topic method does not accept a null object as its parameter", ex.Message);
            }
        }

        [TestMethod]
        public void TestIllegalTopic()
        {
            TopicDAO topDAO = new TopicDAO();
            topic tp = new topic();
            tp.subcontent = "test";
            tp.inUse = false;
            try
            {
                topDAO.InsertTopic(tp);
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual("Verify that all mandatory attributes of the topic object are not null", ex.Message);
            }
        }

        [TestMethod]
        public void TestDeleteTopic()
        {
            TopicDAO topDAO = new TopicDAO();
            topic tp = new topic();
            tp.content = "test";
            tp.subcontent = "test";
            tp.inUse = false;
            topic expected = topDAO.InsertTopic(tp);
            int topID = expected.topicID;
            topic actual = new topic();
            actual = topDAO.GetTopic(topID);
            Assert.AreEqual(expected.topicID, actual.topicID);
            Assert.AreEqual(expected.content, actual.content);
            Assert.AreEqual(expected.subcontent, actual.subcontent);

            topDAO.DeleteTopic(topID);
            actual = topDAO.GetTopic(topID);
            Assert.IsNull(actual);
        }

        [TestMethod]
        public void TestUpdateTopic()
        {
            TopicDAO topDAO = new TopicDAO();
            topic tp = new topic();
            tp.content = "test";
            tp.subcontent = "test";
            tp.inUse = false;
            topic expected = topDAO.InsertTopic(tp);
            int topID = expected.topicID;
            topic actual = new topic();
            actual = topDAO.GetTopic(topID);
            Assert.AreEqual(expected.topicID, actual.topicID);
            Assert.AreEqual(expected.content, actual.content);
            Assert.AreEqual(expected.subcontent, actual.subcontent);

            List<Expression<Func<topic, object>>> attr = new List<Expression<Func<topic, object>>>();
            tp.inUse = true;
            attr.Add(top => top.inUse);

            topDAO.UpdateTopic(expected, attr);

            Assert.AreEqual(expected.inUse, true);

            attr = new List<Expression<Func<topic, object>>>();
            tp.inUse = false;
            attr.Add(top => top.inUse);

            topDAO.UpdateTopic(expected, attr);

            Assert.AreEqual(expected.inUse, false);

            using (var db = new soen390w14teamdEntities())
            {
                db.Database.ExecuteSqlCommand("DELETE FROM topics WHERE topicId={0}", topID);
            }
        }
    
        [TestMethod]
        public void TestGetTopicList()
        {
            List<topic> lsTopic = new List<topic>();
            TopicDAO topDAO = new TopicDAO();
            topic tp = new topic();
            tp.content = "one";
            tp.subcontent = "one";
            tp.inUse = false;
            topic expected = topDAO.InsertTopic(tp);
            int topID = expected.topicID;

            topic tpTwo = new topic();
            tpTwo.content = "two";
            tpTwo.subcontent = "two";
            tpTwo.inUse = false;
            topic expectedTwo = topDAO.InsertTopic(tpTwo);
            int topTwoID = expectedTwo.topicID;

            List<topic> expectTopic = new List<topic>();
            expectTopic.Add(expected);
            expectTopic.Add(expectedTwo);

            List<topic> actualTopic = new List<topic>();

            lsTopic = topDAO.GetTopicList();
            foreach(topic top in lsTopic)
            {
                if(top.topicID == topID || top.topicID == topTwoID)
                {
                    actualTopic.Add(top);
                }
            }

            Assert.AreEqual(expectTopic[0].topicID, actualTopic[0].topicID);
            Assert.AreEqual(expectTopic[1].topicID, actualTopic[1].topicID);

            using (var db = new soen390w14teamdEntities())
            {
                db.Database.ExecuteSqlCommand("DELETE FROM topics WHERE topicId={0}", topID);
                db.Database.ExecuteSqlCommand("DELETE FROM topics WHERE topicId={0}", topTwoID);
            }
        }
    
    }
}

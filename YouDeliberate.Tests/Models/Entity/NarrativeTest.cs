﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.Services;

namespace YouDeliberate.Tests.Models
{
    [TestClass]
    public class NarrativeTest
    {
        [TestMethod]
        public void TestGetPlayList()
        {
            narrative n = new narrative();

            for(int i = 0; i < 3; i++)
            {
                audio a = new audio();
                a.path = i.ToString() + ".mp3";
                n.audios.Add(a);
            }

            for(int i = 0; i < 2; i++)
            {
                picture p = new picture();
                p.path = i.ToString() + ".jpg";
                n.pictures.Add(p);
            }

            List<Media> expected = new List<Media>();
            expected.Add(new Media(n.audios.First(a => a.path == "0.mp3"), n.pictures.First(p => p.path == "0.jpg")));
            expected.Add(new Media(n.audios.First(a => a.path == "1.mp3"), n.pictures.First(p => p.path == "1.jpg")));
            expected.Add(new Media(n.audios.First(a => a.path == "2.mp3"), n.pictures.First(p => p.path == "1.jpg")));

            List<Media> result = n.GetPlayList();

            Assert.IsTrue(expected.SequenceEqual(result));
        }

        [TestMethod]
        public void TestGetPlayListSinglePicture()
        {
            narrative n = new narrative();

            for(int i = 0; i < 3; i++)
            {
                audio a = new audio();
                a.path = i.ToString() + ".mp3";
                n.audios.Add(a);
            }

            picture p = new picture { path = "3.jpg" };
            n.pictures.Add(p);

            List<Media> expected = new List<Media>();
            foreach (audio audio in n.audios)
            {
                expected.Add(new Media(audio, p));
            }

            List<Media> result = n.GetPlayList();

            Assert.IsTrue(expected.SequenceEqual(result));
        }

        [TestMethod]
        public void TestVote()
        {
            Expression<Func<narrative, object>> agreeExpected = (n => n.numberAgrees);
            Expression<Func<narrative, object>> disagreeExpected = (n => n.numberDisagrees);
            Expression<Func<narrative, object>> ambivalentExpected = (n => n.numberAmbivalent);
            narrative result = new narrative();
            int agrees = result.numberAgrees;
            int disagrees = result.numberDisagrees;
            int ambivalents = result.numberAmbivalent;


            Expression<Func<narrative, object>> agree = result.Vote("Agree");
            Expression<Func<narrative, object>> disagree = result.Vote("Disagree");
            Expression<Func<narrative, object>> ambivalent = result.Vote("Ambivalent");

            Assert.AreEqual(agreeExpected.ToString(), agree.ToString());
            Assert.AreEqual(agrees + 1, result.numberAgrees);

            Assert.AreEqual(disagreeExpected.ToString(), disagree.ToString());
            Assert.AreEqual(disagrees + 1, result.numberDisagrees);

            Assert.AreEqual(ambivalentExpected.ToString(), ambivalent.ToString());
            Assert.AreEqual(ambivalents + 1, result.numberAmbivalent);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestFaultyVote()
        {
            narrative n = new narrative();
            n.Vote("EXCEPTION");
        }
    }
}

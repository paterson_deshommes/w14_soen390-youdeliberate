﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using YouDeliberate.Controllers;
using YouDeliberate.Models.DAO;
using YouDeliberate.Models.Entity;




namespace YouDeliberate.Tests.Controllers
{
    [TestClass]
    public class XMLModelTest
    {
        [TestMethod]
        public void TestParseXML()
        {
            string xml = "<narrative>" +
                            "<narrativeName>name</narrativeName>" +
                            "<language>English</language>" +
                            "<submitDate>1970-01-01</submitDate>" +
                            "<time>00-00-00</time>" +
                          "</narrative>";
            XMLModel xmlModel = XMLModel.ParseXML(xml);
            Assert.AreEqual("English", xmlModel.language);
            Assert.AreEqual("name", xmlModel.narrativeName);
            Assert.AreEqual("1970-01-01", xmlModel.submitDate);
            Assert.AreEqual("00-00-00", xmlModel.time);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestInvalidParseXML()
        {
            string xml = "<abc>" +
                            "<narrativeName>name</narrativeName>" +
                            "<language>English</language>" +
                            "<submitDate>1970-01-01</submitDate>" +
                            "<time>00-00-00</time>" +
                          "</abc>";
            XMLModel.ParseXML(xml);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestEmptyParseXML()
        {
            XMLModel.ParseXML("");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestNullParseXML()
        {
            XMLModel.ParseXML(null);
        }

        [TestMethod]
        public void TestXMLToDB()
        {
            string xml = "<narrative>" +
                            "<narrativeName>name</narrativeName>" +
                            "<language>English</language>" +
                            "<submitDate>1970-01-01</submitDate>" +
                            "<time>00-00-00</time>" +
                          "</narrative>";
            XMLModel xmlModel = XMLModel.ParseXML(xml);

            Mock<ILanguageDAO> langDAO = new Mock<ILanguageDAO>();
            language english = new language();
            english.language1 = "English";
            english.languageID = 1;

            langDAO.Setup(dao => dao.retrieveLanguageByName("English")).Returns(english);

            Mock<ICategoryDAO> catDAO = new Mock<ICategoryDAO>();
            category none = new category();
            none.category1 = "None";
            none.categoryID = 4;

            catDAO.Setup(dao => dao.retrieveCategoryByName("None")).Returns(none);

            Mock<INarrativeDAO> narDAO = new Mock<INarrativeDAO>();

            xmlModel.narDAO = narDAO.Object;
            xmlModel.langDAO = langDAO.Object;
            xmlModel.categDAO = catDAO.Object;
            narrative result = xmlModel.XMLtoDB("TEST_PATH");

            Assert.AreEqual("TEST_PATH", result.path);
            Assert.AreEqual("name", result.name);
            Assert.AreEqual(1, result.languageID);
            Assert.AreEqual(4, result.categoryID);
        }
    }
}

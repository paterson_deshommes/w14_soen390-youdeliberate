﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using YouDeliberate.Models.Services;
using Moq;
using System.Web;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Tests.Models
{
    [TestClass]
    public class AuthServiceTest
    {
        private readonly string ADMIN = "admin";
        private AuthService authService = new AuthService();

        [TestMethod]
        public void TestBadLogin()
        {
            Assert.IsFalse(authService.Login("test", "test", null));
        }

        [TestMethod]
        public void TestLogin()
        {
            AdminService adminService = new AdminService();
            Mock<HttpSessionStateBase> session = new Mock<HttpSessionStateBase>();

            administrator admin = adminService.AddAdministrator("test", "test", "test");
            Assert.IsTrue(authService.Login("test", "test", session.Object));

            adminService.DeleteAdministrator(admin.adminID);
        }

        [TestMethod]
        public void TestVerify()
        {
            Mock<HttpSessionStateBase> session = new Mock<HttpSessionStateBase>();
            session.Setup(s => s[ADMIN]).Returns("test");

            Assert.AreEqual("Upload", authService.VerifyLogin("Upload", session.Object));
        }

        [TestMethod]
        public void TestVerifyFail()
        {
            Mock<HttpSessionStateBase> session = new Mock<HttpSessionStateBase>();
            session.Setup(s => s[ADMIN]).Returns(null);

            Assert.AreEqual("Index", authService.VerifyLogin("Upload", session.Object));
        }

        [TestMethod]
        public void TestLogout()
        {
            Mock<HttpSessionStateBase> session = new Mock<HttpSessionStateBase>();
            authService.Logout(session.Object);

            session.Verify(s => s.Remove(ADMIN));
            session.Verify(s => s.Clear());
        }
    }
}

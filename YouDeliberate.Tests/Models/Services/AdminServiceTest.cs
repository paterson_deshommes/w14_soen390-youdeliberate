﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using YouDeliberate.Models.Services;
using YouDeliberate.Models.DAO;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Tests.Models
{
    [TestClass]
    public class AConfigTest
    {
        private string username;
        private string password;
        private string email;

        public AConfigTest()
        {
            username = "dada";
            EncryptService encrypt = new EncryptService();
            password = encrypt.Encrypt("dada");
            email = "dada@hotmail.com";
        }
        
        [TestMethod]
        public void TestAddAdministrator()
        {
            AdminService adminService = new AdminService();
            AdminDAO adminDao = new AdminDAO();

            administrator admin = adminService.AddAdministrator(username, "dada", "dada@hotmail.com");
            Assert.AreEqual("dada",adminDao.RetrieveAdmin(username,password).username);

            adminService.DeleteAdministrator(admin.adminID);
        }

        [TestMethod]
        public void TestModifyAdministrator()
        {
            AdminService adminService = new AdminService();
            AdminDAO adminDao = new AdminDAO();

            administrator admin = adminService.AddAdministrator(username, password, email);
            string oldPass = admin.password;

            adminService.ModifyAdministrator(username, "newPass", "newEmail");
            Assert.IsNull(adminDao.RetrieveAdmin(username,oldPass));

            adminService.DeleteAdministrator(adminDao.GetAdminId(username));
        }

        [TestMethod]
        public void TestDelAdministrator()
        {
            AdminService adminService = new AdminService();
            AdminDAO adminDao = new AdminDAO();

            administrator admin = adminService.AddAdministrator(username, password, email);
            adminService.DeleteAdministrator(admin.adminID);
            Assert.IsNull(adminDao.RetrieveAdmin(username, password));
        }
    }
}

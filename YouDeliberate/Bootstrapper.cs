using Microsoft.Practices.Unity;
using System.Web.Mvc;
using Unity.Mvc4;
using YouDeliberate.Models.DAO;
using YouDeliberate.Models.Services;

namespace YouDeliberate
{
  public static class Bootstrapper
  {
    public static IUnityContainer Initialise()
    {
      var container = BuildUnityContainer();

      DependencyResolver.SetResolver(new UnityDependencyResolver(container));

      return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();

      // register all your components with the container here
      // it is NOT necessary to register your controllers
      container.RegisterType<INarrativeService, NarrativeService>();
      container.RegisterType<IUploadService, UploadService>();
      container.RegisterType<IFileService, FileService>();
      container.RegisterType<IAuthService, AuthService>();

      container.RegisterType<IAdminDAO, AdminDAO>();
      container.RegisterType<INarrativeDAO, NarrativeDAO>();
      container.RegisterType<ICategoryDAO, CategoryDAO>();
      container.RegisterType<ITopicDAO, TopicDAO>();
      // e.g. container.RegisterType<ITestService, TestService>();    
      RegisterTypes(container);

      return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
    
    }
  }
}
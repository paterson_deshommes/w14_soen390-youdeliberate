﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web;
using System.Net.Http;
using YouDeliberate.Models.DAO;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.Services;

using System.Web.Script.Serialization;
using System;

namespace YouDeliberate.Controllers
{
    public class BrowseController : Controller
    {
        private INarrativeService dataService;

        public BrowseController(INarrativeService dataService)
        {
            this.dataService = dataService;
        }

        public ActionResult Index()
        {
            List<narrative> l;
            l = dataService.GetNarratives(viewable: true);
            return View("Index", l);
        }

        [HttpGet]
        public ActionResult SearchList(int id)
        {
            List<Media> l = null;
            try
            {
                l = dataService.GetSearchList(id);
                if (l == null)
                {
                    throw new NullReferenceException();
                }
            }
            catch (NullReferenceException)
            {
                return new HttpStatusCodeResult(550);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetPlaylist(int id)
        {
            List<Media> l = null;
            List<string> returnme = null;
            try
            {

                l = dataService.GetPlayList(id);
                returnme = new List<string>();
                if (l == null)
                    throw new NullReferenceException();

                foreach (var item in l)
                {
                    returnme.Add(item.Audio.path + "");
                    returnme.Add(item.Picture.path + "");
                }

            }
            catch (NullReferenceException)
            {
                return new HttpStatusCodeResult(550);
            }

            return Json(returnme, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SortViewNarratives(string sortType, string langType, string filtType)
        {
            List<narrative> l;
            if (!String.IsNullOrEmpty(filtType))
            {
                ViewBag.PreviousFilterType = filtType;
            }
            if (!String.IsNullOrEmpty(langType))
            {
                ViewBag.PreviousLangType = langType;
            }

            l = dataService.GetNarratives(viewable : true); 

            if (!String.IsNullOrEmpty(sortType))
            {
                ViewBag.PreviousSortType = sortType;
                l = dataService.SortNarratives(l, sortType);
            }
            return View("Index", l);
        }
    }
}

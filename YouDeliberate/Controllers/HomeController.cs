﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YouDeliberate.Models.Services;

namespace YouDeliberate.Controllers
{
    public class HomeController : Controller
    {
        ITopicService ts;

        public HomeController()
        {
            ts = new TopicService();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetTopic()
        {
            List<string> topList = ts.GetCurrentTopic();
            return Json(topList, JsonRequestBehavior.AllowGet);
        }
    }
}

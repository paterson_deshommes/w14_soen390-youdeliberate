﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.Services;

namespace YouDeliberate.Controllers
{
    public class NarrativeController : Controller
    {
        private INarrativeService dataService;

        public NarrativeController(INarrativeService dataService)
        {
            this.dataService = dataService;
        }

        //
        // POST: /Narrative/Vote
        [HttpPost]
        public ActionResult Vote(string type, int nid)
        {
            try
            {
                narrative n = dataService.GetNarrative(nid);
                Expression<Func<narrative, object>> vote = n.Vote(type);

                List<Expression<Func<narrative, object>>> attr = new List<Expression<Func<narrative, object>>>();
                attr.Add(vote);

                dataService.Save(n, attr);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(500);
            }
            return new HttpStatusCodeResult(200);
        }

        [HttpGet]
        public ActionResult LoadPlayer(int id)
        {
            narrative n = dataService.GetNarrative(id);
            if (n == null)
            {
                return new HttpStatusCodeResult(550); 
            }
            Dictionary<string, object> returnme = new Dictionary<string, object>();
            if(n.comments.Count == 0)
            {
                returnme.Add("comments", (n.comments.Count));
            }
            else
            {
                returnme.Add("comments", (n.comments.Count-1));
            }
            returnme.Add("agrees", n.numberAgrees);
            returnme.Add("disagrees", n.numberDisagrees);
            returnme.Add("ambivalent", n.numberAmbivalent);
            returnme.Add("views", n.numberViews);

            return Json(returnme, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult View(int id)
        {
            try
            {
                narrative n = dataService.GetNarrative(id);

                n.numberViews++;

                List<Expression<Func<narrative, object>>> attr = new List<Expression<Func<narrative, object>>>();
                attr.Add(narr => narr.numberViews);

                dataService.Save(n, attr);
            }
            catch (Exception)
            {
                return new HttpStatusCodeResult(500);
            }
            return new HttpStatusCodeResult(200);
        }

        [HttpGet]
        public ActionResult GetComments(int id)
        {
            return Json(dataService.GetComments(id), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetChildComs(int nid, int cid)
        {
            List<string> coms = dataService.GetChildComments(nid, cid);
            return Json(coms, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public string AddComment(int id, string c, HttpRequestMessage request)
        {
            return dataService.AddComment(id, c, GetClientIp(request));
        }
        private static string GetClientIp(HttpRequestMessage request)
        {
            string ip = string.Empty;
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                HttpContextBase context = (HttpContextBase)request.Properties["MS_HttpContext"];
                if (context.Request.ServerVariables["HTTP_VIA"] != null)
                {
                    ip = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                }
                else
                {
                    ip = context.Request.ServerVariables["REMOTE_ADDR"].ToString();
                }
            }
            return ip;

        }

        [HttpGet]
        public ActionResult AddComOnComment(int narid, int id, String com, HttpRequestMessage request)
        {
            dataService.AddChildCom(narid, id, com, GetClientIp(request));
            return Json(id, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public bool AddFlag(int id, string c)
        {
            if (c != null && c != "")
                return dataService.AddFlag(id, c);

            return false;
        }
    }
}
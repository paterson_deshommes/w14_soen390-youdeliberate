﻿using System;
using System.Linq;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.DAO
{
    public class CategoryDAO : ICategoryDAO
    {
        public category retrieveCategoryByID(int id)
        {
            using (var db = new soen390w14teamdEntities())
            {
                try {
                    category cat = db.categories.First(c => c.categoryID == id);
                    return cat;
                }
                catch (InvalidOperationException )
                {
                    return null;
                }
            }
        }

        public category retrieveCategoryByName(String name)
        {
            using (var db = new soen390w14teamdEntities())
            {
                try
                {
                    category cat = db.categories.First(c => c.category1.Equals(name));
                    return cat;
                }
                catch (InvalidOperationException)
                {
                    return null;
                }
            }
        }
    }
}
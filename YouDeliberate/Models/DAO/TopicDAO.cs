﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.DAO
{
    public class TopicDAO : ITopicDAO
    {
        public topic InsertTopic(topic viewpoints)
        {
            if (viewpoints == null)
            {
                throw new ArgumentNullException("topic method does not accept a null object as its parameter");
            }
            bool illegalParameters = (viewpoints.content == null);
            if (illegalParameters)
            {
                throw new ArgumentException("Verify that all mandatory attributes of the topic object are not null");
            }

            using (var db = new soen390w14teamdEntities())
            {
                db.topics.Add(viewpoints);
                db.SaveChanges();
                return viewpoints;
            }
        }

        public void DeleteTopic(int id)
        {
            using (var db = new soen390w14teamdEntities())
            {
                var sql = @"DELETE FROM topics WHERE topicID = {0}";
                db.Database.ExecuteSqlCommand(sql, id);
            }
        }

        public topic UpdateTopic(topic tp, List<Expression<Func<topic, object>>> modified)
        {
            using (var db = new soen390w14teamdEntities())
            {
                db.Configuration.ValidateOnSaveEnabled = false;
                db.topics.Attach(tp);
                foreach (Expression<Func<topic, object>> attribute in modified)
                {
                    try
                    {
                        db.Entry(tp).Property(attribute).IsModified = true;
                    }
                    catch (Exception e)
                    {
                        throw new ArgumentOutOfRangeException("Cannot update objects in topic. " + e.Message);
                    }
                }
                db.SaveChanges();
            }
            return tp;
        }

        public List<topic> GetTopicList()
        {
            List<topic> topics = new List<topic>();
            using (var db = new soen390w14teamdEntities())
            {

                try
                {
                    topics =
                    db.topics.SqlQuery("SELECT * FROM topics").ToList();
                }
                catch(Exception)
                {
                    topics = null;
                }
            }
            return topics;
        }

        public topic GetTopic(int id)
        {
            using (var db = new soen390w14teamdEntities())
            {
                try
                {
                    topic top = db.topics.First(t => t.topicID.Equals(id));
                    return top;
                }
                catch (InvalidOperationException)
                {
                    return null;
                }
            }
        }

        public topic GetCurTopic()
        {
            var db = new soen390w14teamdEntities();
            topic top = new topic();
            bool flag = true;

            try
            {
                top = db.topics.First(n => n.inUse == flag);
                return top;
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

    }
}
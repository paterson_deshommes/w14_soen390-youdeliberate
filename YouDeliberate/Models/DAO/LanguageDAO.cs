﻿using System;
using System.Linq;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.DAO
{
    public class LanguageDAO : ILanguageDAO
    {
        public language retrieveLanguageByID(int id)
        {
            using (var db = new soen390w14teamdEntities())
            {
                try
                {
                    language lang = db.languages.First(l => l.languageID == id);
                    return lang;
                }
                catch (InvalidOperationException )
                {
                    return null;
                }
            }
        }

        public language retrieveLanguageByName(String name)
        {
            using (var db = new soen390w14teamdEntities())
            {
                try
                {
                    language lang = db.languages.First(l => l.language1.Equals(name));
                    return lang;
                }
                catch (InvalidOperationException )
                {
                    return null;
                }
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.DAO
{
    public interface ICommentDAO
    {
        /// <summary>
        /// Get the top level comments of a narrative
        /// </summary>
        /// <param name="nid">ID of the narrative</param>
        /// <returns>List of comments sorted by time (descending)</returns>
        List<comment> GetComments(int nid);

        /// <summary>
        /// Get a quantity of comments
        /// </summary>
        /// <param name="start">first comment</param>
        /// <param name="end">last comment</param>
        /// <returns>List of comments sored by time (descending)</returns>
        List<comment> GetComments(int start, int end);
        
        /// <summary>
        /// Get total number of comments
        /// </summary>
        /// <returns>Total Number of Comments</returns>
        int GetNrComments();

        /// <summary>
        /// Get the child comments of a comment
        /// </summary>
        /// <param name="nid">ID of the narrative</param>
        /// <param name="cid">ID of the parent comment</param>
        /// <returns></returns>
        List<comment> GetChildComments(int nid, int cid);

        /// <summary>
        /// Delete a specific comment
        /// </summary>
        /// <param name="id">Comment Id to be deleted</param>
        /// <returns>True/False depending if the operation was succesfull</returns>
        bool DeleteComment(int id);
        
    }
}
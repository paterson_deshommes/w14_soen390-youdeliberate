﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YouDeliberate.Models.DAO
{
    public interface ILanguageDAO
    {
        /// <summary>
        /// Retrieve Language By Name
        /// </summary>
        /// <param name="p">String of The Langauge</param>
        /// <returns>Language Object</returns>
        Entity.language retrieveLanguageByName(string p);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.Filters;

namespace YouDeliberate.Models.DAO
{
    public class NarrativeDAO : INarrativeDAO
    {
        private static readonly string COMMENTS = "comments";
        private static readonly string LANGUAGE = "language";
        private static readonly string CATEGORY = "category";
        private static readonly string AUDIOS = "audios";
        private static readonly string PICTURES = "pictures";
        private static readonly string FLAGS = "narrativeflags";

        public narrative InsertNarrative(narrative viewpoints)
        {
            if (viewpoints == null)
            {
                throw new ArgumentNullException("insertNarrative method does not accept a null object as its parameter");
            }
            bool illegalParameters = (viewpoints.path == null || viewpoints.languageID == 0 || viewpoints.categoryID == 0);
            if (illegalParameters)
            {
                throw new ArgumentException("Verify that all mandatory attributes of the narrative object are not null");
            }

            using (var db = new soen390w14teamdEntities())
            {
                db.narratives.Add(viewpoints);
                db.SaveChanges();
                return viewpoints;
            }
        }

        public narrative RetrieveNarrative(int id)
        {
            //TODO Figure out how we can use a using without getting a objectdisposedexception
            using (var db = new soen390w14teamdEntities())
            {
                try
                {
                    narrative viewpoints = db.narratives
                                                .Include(AUDIOS)
                                                .Include(PICTURES)
                                                .Include(CATEGORY)
                                                .Include(LANGUAGE)
                                                .Include(COMMENTS)
                                                .Include(FLAGS)
                                                .First(n => n.narrativeID == id);
                    return viewpoints;
                }
                catch (InvalidOperationException)
                {
                    return null;
                }
            }
        }

        public List<narrative> RetrieveAllNarratives()
        {
            //TODO Figure out how we can use a using without getting a objectdisposedexception
            using (var db = new soen390w14teamdEntities())
            {
                try
                {
                    List<narrative> viewpoints = db.narratives
                                                .Include(AUDIOS)
                                                .Include(PICTURES)
                                                .Include(CATEGORY)
                                                .Include(LANGUAGE)
                                                .Include(COMMENTS)
                                                .Include(FLAGS).ToList();
                    return viewpoints;
                }
                catch (InvalidOperationException)
                {
                    return null;
                }
            }
        }

        public narrative UpdateNarrative(narrative n, List<Expression<Func<narrative, object>>> modified)
        {
            using (var db = new soen390w14teamdEntities())
            {
                db.Configuration.ValidateOnSaveEnabled = false; // So it doesn't complain about null fields
                db.narratives.Attach(n);
                foreach (Expression<Func<narrative, object>> attribute in modified)
                {
                    try
                    {
                        db.Entry(n).Property(attribute).IsModified = true;
                    }
                    catch (Exception e)
                    {
                        throw new ArgumentOutOfRangeException("Cannot update objects in narrative. " + e.Message);
                    }
                }
                db.SaveChanges();
            }
            return n;
        }

        public ICollection<narrative> GetFilteredList(int a, int b, IEnumerable<IFilter> filters)
        {
            ICollection<narrative> narratives = new List<narrative>();
            using (var db = new soen390w14teamdEntities())
            {
                narratives = db.narratives
                .Include(AUDIOS)
                .Include(PICTURES)
                .Include(CATEGORY)
                .Include(LANGUAGE)
                .Include(COMMENTS)
                .Include(FLAGS)
                    /*
                    .Where(n => n.categoryID != 4)
                    .Where(n => n.flagged == false)
                    .Where(n => n.userViewable == true)
                    */
                .OrderByDescending(n => n.narrativeID).ToList();

                foreach (IFilter filter in filters)
                {
                    narratives = narratives.Intersect(db.narratives
                                            .Include(AUDIOS)
                                            .Include(PICTURES)
                                            .Include(CATEGORY)
                                            .Include(LANGUAGE)
                                            .Include(COMMENTS)
                                            .Include(FLAGS)
                                            .Where(filter.GetFilter())).ToList();
                }

                if (a > 0 && b > a)
                {
                    narratives = narratives.Skip(a).Take(b - a).ToList();
                }
            }
            return narratives;
        }

        public comment AddComment(comment c)
        {
            using (var db = new soen390w14teamdEntities())
            {
                if (c.comments != null || c.content.Length < 512)
                {
                    try
                    {
                        db.comments.Add(c);
                        db.SaveChanges();
                    } catch (Exception)
                    {
                    }
                }
            }
            return c;
        }

        public void AddComOnCom(comment com, int parent)
        {
            using (var db = new soen390w14teamdEntities())
            {
                var sql = @"INSERT INTO commentchildof VALUES ({0} , {1})";
                db.Database.ExecuteSqlCommand(sql, com.commentID,parent);
            }
        }

        public bool AddFlag(narrativeflag f)
        {
            using (var db = new soen390w14teamdEntities())
            {
                db.narrativeflags.Add(f);
                db.SaveChanges();
            }
            return true;
        }

        public void UpdateAudio(narrative n)
        {
            using (var db = new soen390w14teamdEntities())
            {
                narrative tracked = db.narratives.First(narr => narr.narrativeID == n.narrativeID);
                List<audio> toDelete, toAdd;
                if (n.audios == null)
                {
                    toDelete = tracked.audios.ToList();
                    toAdd = new List<audio>();
                }
                else
                {
                    toDelete = tracked.audios.Where(a => !n.audios.Contains(a)).ToList();
                    toAdd = n.audios.Where(a => !tracked.audios.Contains(a)).ToList();
                }

                foreach (audio audio in toDelete)
                {
                    db.Entry(audio).State = EntityState.Deleted;
                }
                foreach (audio audio in toAdd)
                {
                    audio.narrative = tracked;
                    db.audios.Add(audio);
                }
                db.SaveChanges();
            }
        }

        public void UpdatePicture(narrative n)
        {
            using (var db = new soen390w14teamdEntities())
            {
                narrative tracked = db.narratives.First(narr => narr.narrativeID == n.narrativeID);
                List<picture> toDelete, toAdd;
                if (n.pictures == null)
                {
                    toDelete = tracked.pictures.ToList();
                    toAdd = new List<picture>();
                }
                else
                {
                    toDelete = tracked.pictures.Where(p => !n.pictures.Contains(p)).ToList();
                    toAdd = n.pictures.Where(p => !tracked.pictures.Contains(p)).ToList();
                }

                foreach (picture picture in toDelete)
                {
                    db.Entry(picture).State = EntityState.Deleted;
                }
                foreach (picture picture in toAdd)
                {
                    picture.narrative = tracked;
                    db.pictures.Add(picture);
                }
                db.SaveChanges();
            }
        }
    }
}
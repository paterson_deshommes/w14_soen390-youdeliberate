﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.DAO
{
    public class AdminDAO : IAdminDAO
    {
        //test AppVeyor
        //private string test;
        public administrator InsertAdmin(administrator viewpoints)
        {
            if (viewpoints == null)
            {
                throw new ArgumentNullException("insertNarrative method does not accept a null object as its parameter");
            }
            bool illegalParameters = (viewpoints.username == null || viewpoints.password == null || viewpoints.email == null);
            if (illegalParameters)
            {
                throw new ArgumentException("Verify that all mandatory attributes of the narrative object are not null");
            }

            using (var db = new soen390w14teamdEntities())
            {
                db.administrators.Add(viewpoints);
                db.SaveChanges();
                return viewpoints;
            }
        }

        public void DeleteAdministrator(int id)
        {
            using (var db = new soen390w14teamdEntities())
            {
                var sql = @"DELETE FROM administrator WHERE adminID = {0}";
                db.Database.ExecuteSqlCommand(sql, id);
            }
        }

        public administrator RetrieveAdmin(String username, String password)
        {
            //TODO Figure out how we can use a using without getting a objectdisposedexception
            var db = new soen390w14teamdEntities();

            try
            {
                administrator viewpoints = db.administrators.First(n => n.username == username && n.password == password);
                return viewpoints;
            }
            catch (InvalidOperationException)
            {
                return null;
            }
        }

        public int GetAdminId(String username)
        {
            var db = new soen390w14teamdEntities();
            administrator admin = new administrator();

            try
            {
                admin = db.administrators.First(n => n.username == username);
                return admin.adminID;
            }
            catch (InvalidOperationException)
            {
                return 0;
            }
        }

        public void UpdateAdministrator(String username,String password,String email)
        {
            using (var db = new soen390w14teamdEntities())
            {
                var sql = @"UPDATE administrator 
                            SET password = {0}, email = {1}
                            WHERE username = {2}";
                db.Database.ExecuteSqlCommand(sql,password,email,username);
            }
        }

        public List<administrator> GetAdmins()
        {
            List<administrator> admins;
            using(var db = new soen390w14teamdEntities())
            {
                admins = db.administrators.ToList();
            }
            return admins;
        }
    }
}
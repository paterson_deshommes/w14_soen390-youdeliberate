﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YouDeliberate.Models.Entity
{
    public partial class audio : IEquatable<audio>
    {
        public bool Equals(audio other)
        {
            if(other == null)
            {
                return false;
            }
            else
            {
                return other.audioID == this.audioID;
            }
        }
    }
}
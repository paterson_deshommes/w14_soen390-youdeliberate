﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YouDeliberate.Models.Entity
{
    public partial class picture : IEquatable<picture>
    {
        public bool Equals(picture other)
        {
            if(other == null)
            {
                return false;
            }
            else
            {
                return other.pictureID == this.pictureID;
            }
        }
    }
}
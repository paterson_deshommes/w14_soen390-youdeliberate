﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YouDeliberate.Models.Entity
{
    public class Media : IEquatable<Media>
    {
        public audio Audio {get; set;}
        public picture Picture {get; set;}

        public Media(audio audio, picture picture)
        {
            this.Audio = audio;
            this.Picture = picture;
        }

        public bool Equals(Media other)
        {
            if(other == null)
            {
                return false;
            }
            else
            {
                return (this.Audio.path == other.Audio.path && 
                        this.Picture.path == other.Picture.path);
            }
        }
    }
}
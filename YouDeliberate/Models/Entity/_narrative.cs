﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace YouDeliberate.Models.Entity
{
    public partial class narrative : IEquatable<narrative>
    {
        private static readonly
        Dictionary<string, Func<narrative, Expression<Func<narrative, object>>>> votes
        = new Dictionary<string, Func<narrative, Expression<Func<narrative, object>>>>
            {
                {"AGREE", n => n.Agree()},
                {"DISAGREE", n => n.Disagree()},
                {"AMBIVALENT", n => n.Ambivalent()}
            };

        public List<Media> GetPlayList()
        {
            List<Media> playList = new List<Media>();
            List<audio> sortedAudio = this.audios.ToList();
            sortedAudio.Sort(delegate (audio a1, audio a2)
                                            {
                                                return a1.path.CompareTo(a2.path);
                                            });

            List<picture> sortedPictures = this.pictures.ToList();
            sortedPictures.Sort(delegate(picture p1, picture p2)
                                            {
                                                return p2.path.CompareTo(p1.path);
                                            });

            foreach(audio audio in sortedAudio)
            {
                playList.Add(FindAssociatedPicture(audio, sortedPictures));
            }
            return playList;
        }

        private Media FindAssociatedPicture(audio audio, List<picture> pictures)
        {
            string audioName = audio.path.Substring(0, audio.path.LastIndexOf('.'));
            foreach(picture picture in pictures)
            {
                string pictureName = picture.path.Substring(0, picture.path.LastIndexOf('.'));
                if(audioName.CompareTo(pictureName) >= 0)
                {
                    return new Media(audio, picture);
                }
            }

            // Combine audio with first picture
            picture p = new picture();
            try {
                p = pictures.Last();
            }
            catch (InvalidOperationException) { }

            return new Media(audio, p);
        }

        /// <summary>
        /// Modify the proper vote attribute and return the expression to save it
        /// </summary>
        /// <param name="voteType">String of the vote (Agree, Disagree, or Ambivalent)</param>
        /// <returns>The LINQ Expression to save to the database</returns>
        public Expression<Func<narrative, object>> Vote(string voteType)
        {
            voteType = voteType.ToUpper();
            if (!votes.ContainsKey(voteType))
            {
                throw new ArgumentOutOfRangeException("Vote type not recognized.");
            }

            return votes[voteType].Invoke(this);
        }

        private Expression<Func<narrative, object>> Agree()
        {
            this.numberAgrees++;
            Expression<Func<narrative, object>> agrees = n => n.numberAgrees;
            return agrees;
        }

        private Expression<Func<narrative, object>> Ambivalent()
        {
            this.numberAmbivalent++;
            Expression<Func<narrative, object>> ambivalent = n => n.numberAmbivalent;
            return ambivalent;
        }

        private Expression<Func<narrative, object>> Disagree()
        {
            this.numberDisagrees++;
            Expression<Func<narrative, object>> disagrees = n => n.numberDisagrees;
            return disagrees;
        }

        public bool Equals(narrative narrative)
        {
            return this.narrativeID == narrative.narrativeID;
        }

        public override int GetHashCode()
        {
            return this.narrativeID.GetHashCode();
        } 

    }
}
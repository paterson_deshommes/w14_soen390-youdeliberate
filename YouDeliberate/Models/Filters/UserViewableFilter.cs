﻿using System;
using System.Linq.Expressions;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.Filters
{
    public class UserViewableFilter : IFilter
    {
        private bool UserViewable { get; set; }

        public UserViewableFilter(bool b)
        {
            UserViewable = b;
        }

        public Expression<Func<narrative, bool>> GetFilter()
        {
            return n => n.userViewable == UserViewable;
        }
    }
}
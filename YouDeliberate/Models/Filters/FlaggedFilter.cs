﻿using System;
using System.Linq.Expressions;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.Filters
{
    public class FlaggedFilter : IFilter
    {
        private bool IsGreaterThan { get; set; }
        private int NumberOfFlags { get; set; }

        /// <summary>
        /// Creates a filter to test if the number of flags of a narrative IsGreaterThan (if b == true) NumberOfFlags
        /// </summary>
        /// <param name="b"></param>
        /// <param name="i"></param>
        public FlaggedFilter(bool b, int i)
        {
            IsGreaterThan = b;
            NumberOfFlags = i;
        }

        public Expression<Func<narrative, bool>> GetFilter()
        {
            if (IsGreaterThan)
            {
                return n => (n.narrativeflags.Count > NumberOfFlags);
            }
            return n => (n.narrativeflags.Count < NumberOfFlags);
        }
    }
}
﻿using System;
using System.Linq.Expressions;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.Filters
{
    public class LanguageFilter : IFilter
    {
        private string Language;
        private int LanguageID;

        public LanguageFilter(string language)
        {
            Language = language;
        }

        public LanguageFilter(int languageID)
        {
            LanguageID = languageID;
        }

        public Expression<Func<narrative, bool>> GetFilter()
        {
            Expression<Func<narrative, bool>> expr = null;
            if(Language != null)
            {
                expr = n => n.language.language1.Equals(Language);
            }
            else if(LanguageID > 0)
            {
                expr = n => n.language.languageID.Equals(LanguageID);
            }
            return expr;
        }
    }
}
﻿using System;
using System.IO;
using System.IO.Compression;
using System.Web;
using SystemWrapper.IO;

namespace YouDeliberate.Models.Services
{
    public class FileService : IFileService
    {
        public string ExtractFile(HttpPostedFileBase file, IDirectoryInfoWrap directoryPath, IDirectoryInfoWrap narPath)
        {
            if (file != null && file.ContentLength > 0)
            {
                PathWrap pathWrap = new PathWrap();
                string path = pathWrap.Combine(directoryPath.ToString(), file.FileName);
                file.SaveAs(path);
                DirectoryInfoWrap zipPath = new DirectoryInfoWrap(path);
                if (zipPath.Extension == ".zip")
                {
                    ZipFile.ExtractToDirectory(path, narPath.ToString());
                    return path;
                }
                else
                {
                    throw new InvalidOperationException("Invalid type of file submitted.");
                }
            }
            else
            {
                throw new InvalidOperationException("There is no zip file submited.");
            }
        }

        public void Delete(string path)
        {
            if (path != null)
            {
                try
                {
                    FileAttributes attr = File.GetAttributes(path);
                    if (FileAttributes.Directory == attr)
                    {
                        DirectoryInfoWrap directory = new DirectoryInfoWrap(path);
                        directory.Delete(true);
                    }
                    else
                    {
                        File.Delete(path);
                    }
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
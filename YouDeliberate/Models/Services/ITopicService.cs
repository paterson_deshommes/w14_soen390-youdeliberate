﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using YouDeliberate.Models.DAO;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.Services
{
    public interface ITopicService
    {

        /// <summary>
        /// Retrieve Current Topic
        /// </summary>
        /// <returns>String List of The Topic</returns>
        List<string> GetCurrentTopic();

        /// <summary>
        /// Retrieve List of All Topics
        /// </summary>
        /// <returns>String List of Topics</returns>
        List<string> GetListAllTopics();

        /// <summary>
        /// Add a Topic
        /// </summary>
        /// <param name="bgImg">Background Image</param>
        /// <param name="enVid">English Video</param>
        /// <param name="frVid">French Video</param>
        /// <param name="topic">Topic Content</param>
        /// <param name="subtopic">Subtopic Content</param>
        void AddTopic(HttpPostedFile bgImg, HttpPostedFile enVid, HttpPostedFile frVid, topic topic);

        /// <summary>
        /// Update a Topic to be Displayed on Home Page
        /// </summary>
        /// <param name="id">Topic Id</param>
        topic ChangeOnlineTopic(int id);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouDeliberate.Models.DAO;

namespace YouDeliberate.Models.Services
{
    public class ServiceRegistry
    {
        public ServiceRegistry()
        {
            AdminService = new AdminService();
            AuthService = new AuthService();
            FileService = new FileService();
            NarrativeService = new NarrativeService();
            UploadService = new UploadService();
            TopicService = new TopicService();
        }
        public IAdminService AdminService { get; set; }
        public IAuthService AuthService {get; set;}
        public IFileService FileService {get; set;}
        public INarrativeService NarrativeService { get; set; }
        public IUploadService UploadService { get; set; }
        public ITopicService TopicService { get; set; }
    }
}
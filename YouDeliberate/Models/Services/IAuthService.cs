﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YouDeliberate.Models.Services
{
    public interface IAuthService
    {
        /// <summary>
        /// Verify Login
        /// </summary>
        /// <param name="p">View</param>
        /// <param name="Session">Session</param>
        /// <returns>ActionResult View String</returns>
        string VerifyLogin(string p, System.Web.HttpSessionStateBase Session);

        /// <summary>
        /// Logout
        /// </summary>
        /// <param name="Session">Current Session</param>
        void Logout(System.Web.HttpSessionStateBase Session);

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="p1">Username</param>
        /// <param name="p2">Password</param>
        /// <param name="Session">Session</param>
        /// <returns></returns>
        bool Login(string p1, string p2, System.Web.HttpSessionStateBase Session);
    }
}

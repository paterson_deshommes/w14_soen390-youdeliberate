﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.Services
{
    public interface INarrativeService
    {
        /// <summary>
        /// Update Narrative
        /// </summary>
        /// <param name="n">Narrative Object to be Modified</param>
        /// <param name="attr">List of Narrative Object Attributes</param>
        /// <returns>Modified Narrative</returns>
        narrative Save(narrative n, List<Expression<Func<narrative, object>>> attr);

        /// <summary>
        /// Return a Narrative
        /// </summary>
        /// <param name="nid">Narrative Id</param>
        /// <returns>Narrative Object</returns>
        narrative GetNarrative(int nid);

        /// <summary>
        /// Update Audio
        /// </summary>
        /// <param name="n">Narrative Object</param>
        void UpdateAudio(narrative n);

        /// <summary>
        /// Update Picture
        /// </summary>
        /// <param name="n">Narrative Object</param>
        void UpdatePicture(narrative n);

        /// <summary>
        /// Add New Comment
        /// </summary>
        /// <param name="id">Comment Id</param>
        /// <param name="s">Comment Content</param>
        /// <param name="ip">Comment Ip</param>
        /// <returns>Comment Hash</returns>
        string AddComment(int id, string s, string ip);

        /// <summary>
        /// Add Child Comment
        /// </summary>
        /// <param name="narid">Narrative Id</param>
        /// <param name="id">Comment Id</param>
        /// <param name="s">Comment Content</param>
        /// <param name="ip">Comment Ip</param>
        void AddChildCom(int narid, int id, string s, string ip);

        /// <summary>
        /// Get List of comments
        /// </summary>
        /// <param name="id">Comment Id</param>
        /// <returns>String List of Comments</returns>
        List<string> GetComments(int id);

        /// <summary>
        /// Get Comments Pagination
        /// </summary>
        /// <param name="start">Number of Comment to Start</param>
        /// <param name="end">Number of Comment to End</param>
        /// <returns>String List of Comments</returns>
        List<string> GetComments(int start,int end);

        /// <summary>
        /// Retrieve Number of Comments
        /// </summary>
        /// <returns>Total Number of Comments</returns>
        int GetNrComments();

        /// <summary>
        /// Retrieve Number of Child Comments
        /// </summary>
        /// <param name="nid">Narrative Id</param>
        /// <param name="cid">Comment Id</param>
        /// <returns>String List of Child Comments for a Specific Comment</returns>
        List<String> GetChildComments(int nid, int cid);

        /// <summary>
        /// Insert Flag for Narrative
        /// </summary>
        /// <param name="id">Narrative Id</param>
        /// <param name="s">Flag Content</param>
        /// <returns></returns>
        bool AddFlag(int id, string s);

        /// <summary>
        /// Return the Play List
        /// </summary>
        /// <param name="id">Narrative Id</param>
        /// <returns>Video and Audio for a Narrative</returns>
        List<Media> GetPlayList(int id);

        /// <summary>
        /// Search and Retrieve Narrative
        /// </summary>
        /// <param name="id">Narrative Id</param>
        /// <returns>Video and Audio for a Narrative</returns>
        List<Media> GetSearchList(int id);

        /// <summary>
        /// Sort Narratives
        /// </summary>
        /// <param name="l">List of Narratives</param>
        /// <param name="sortby">Type of Sorting</param>
        /// <returns>List of Narratives</returns>
        List<narrative> SortNarratives(List<narrative> l, String sortby);

        /// <summary>
        /// Retrieve the Flags for Narrative
        /// </summary>
        /// <param name="id">Narrative Id</param>
        /// <returns>String List of Flags</returns>
        List<string> GetFlags(int id);

        /// <summary>
        /// Delete Comment
        /// </summary>
        /// <param name="id">Comment Id</param>
        /// <returns>True/False if its Succesfull or Not</returns>
        bool DelComment(int id);

        /// <summary>
        /// Retrieve Narrative
        /// </summary>
        /// <param name="from">Start Narrative Number</param>
        /// <param name="to">Last Narrative Number</param>
        /// <param name="viewable">If Narrative is Viewable</param>
        /// <param name="flagged">If Narrative is Flagged</param>
        /// <param name="uncategorized">If Narrative is Uncategorized</param>
        /// <returns>List of Narratives</returns>
        List<narrative> GetNarratives(int from = 0, int to = 0, bool viewable = false, bool flagged = false, bool uncategorized = false);

        int GetTotalViews();
        int GetTotalComments();
        int GetTotalFlagged();
    }
}

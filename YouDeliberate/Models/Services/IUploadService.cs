﻿using SystemWrapper.IO;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.Services
{
    public interface IUploadService
    {
        /// <summary>
        /// Get narratives from a temporary directory and put them in the database and permanent directory.
        /// </summary>
        /// <param name="directoryPath">Directory where all the narratives are</param>
        /// <param name="narPath">Temporary path for DB</param>
        /// <param name="narrFiles">All the seperate narrative relative paths from directoryPath</param>
        void UploadNarratives(IDirectoryInfoWrap directoryPath, IDirectoryInfoWrap narPath, string[] narrFiles);

        /// <summary>
        /// Validate XML Input
        /// </summary>
        /// <param name="narr">XMLModel</param>
        /// <param name="path">Directory Path</param>
        void ValidateXMLInput(XMLModel narr, IDirectoryInfoWrap path);

        /// <summary>
        /// Add Audio Files
        /// </summary>
        /// <param name="narrative">Narrative Object</param>
        /// <param name="narrativePath">Narrative Path</param>
        void AddAudioFiles(narrative narrative, IDirectoryInfoWrap narrativePath);

        /// <summary>
        /// Add Pictures Files
        /// </summary>
        /// <param name="narrative">Narrative Object</param>
        /// <param name="narrativePath">Narrative Path</param>
        void AddPictureFiles(narrative narrative, IDirectoryInfoWrap narrativePath);

        /// <summary>
        /// Get all the files that have the same extension as given in the parameter
        /// </summary>
        /// <param name="path">The path to look for the files</param>
        /// <param name="extensions">Extensions to look for</param>
        /// <returns>Full path of all the files with the correct extensions</returns>
        string[] GetFilesFromDirectory(IDirectoryInfoWrap path, string[] extensions);
    }
}

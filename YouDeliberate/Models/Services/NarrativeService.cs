﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography;
using System.Text;
using YouDeliberate.Models.DAO;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.Filters;

namespace YouDeliberate.Models.Services
{
    public class NarrativeService : INarrativeService
    {
        private INarrativeDAO NarrativeDAO;
        private ICommentDAO CommentDAO;
        private CategoryDAO CategoryDAO = new CategoryDAO();

        public NarrativeService()
        {
            NarrativeDAO = new NarrativeDAO();
        }

        public NarrativeService(INarrativeDAO indao)
        {
            NarrativeDAO = indao;
        }

        public void AddChildCom(int narid, int id, string s, string ip)
        {
            comment c = new comment();
            c = newComment(narid, s, ip);
            comment newC = NarrativeDAO.AddComment(c);
            NarrativeDAO.AddComOnCom(newC, id);
        }

        public comment newComment(int id, string s, string ip)
        {
            comment c = new comment();
            c.narrativeID = id;
            c.content = s;
            c.timestamp = DateTime.Now;

            byte[] ipp = new UTF8Encoding().GetBytes(ip);
            byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(ipp);
            string encoded = BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
            c.hash = encoded;
            c.flagged = "False";
            return c;
        }

        public string AddComment(int id, string s, string ip)
        {
            comment c = new comment();
            c = newComment(id, s, ip);
            NarrativeDAO.AddComment(c);

            return c.hash;
        }

        public bool AddFlag(int id, string s)
        {
            narrativeflag f = new narrativeflag();
            f.narrativeID = id;
            f.content = s;
            return NarrativeDAO.AddFlag(f);
        }

        public List<string> GetComments(int id)
        {
            narrative n = NarrativeDAO.RetrieveNarrative(id);
            CommentDAO = new CommentDAO();
            List<comment> comList = CommentDAO.GetComments(id);

            return GetComList(comList);
        }

        public List<string> GetComments(int start,int end)
        {
            CommentDAO = new CommentDAO();
            List<comment> comList = CommentDAO.GetComments(start,end);
            return GetComList(comList);
        }

        public int GetNrComments()
        {
            CommentDAO = new CommentDAO();
            int nrCom = CommentDAO.GetNrComments();
            return nrCom;
        }

        public bool DelComment(int id)
        {
            CommentDAO = new CommentDAO();
            return CommentDAO.DeleteComment(id);
        }

        public List<String> GetComList(List<comment> com)
        {
            List<string> l = new List<string>();

            if(com==null)
            {
                return null;
            }

            foreach (comment c in com)
            {
                l.Add(c.hash);
                l.Add(c.content);
                l.Add(c.commentID.ToString());
                var now = DateTime.Now;
                var result = now - c.timestamp;
                var time = getCommentTime(result);
                l.Add(time);
  
            }
            return l;
        }

        public string getCommentTime(TimeSpan result)
        {
            string time = null;

            if (Math.Floor(result.TotalDays) > 0)
            {
                time = Math.Floor(result.TotalDays).ToString() + " days ago";
            }
            else if (Math.Floor(result.TotalHours) > 0)
            {
                time = Math.Floor(result.TotalHours).ToString() + " hours ago";
            }
            else if (Math.Floor(result.TotalMinutes) > 0)
            {
                time = Math.Floor(result.TotalMinutes).ToString() + " minutes ago";
            }
            else
            {
                time = Math.Floor(result.TotalSeconds).ToString() + " seconds ago";
            }

            return time;
        }

        public List<String> GetChildComments(int nid,int cid)
        {
            CommentDAO = new CommentDAO();
            List<comment> child = CommentDAO.GetChildComments(nid, cid);
            return GetComList(child);
        }

        public narrative GetNarrative(int id)
        {
            return NarrativeDAO.RetrieveNarrative(id);
        }

        public List<string> GetFlags(int id)
        {
            narrative n = NarrativeDAO.RetrieveNarrative(id);
            List<string> l = new List<string>();

            foreach (var f in n.narrativeflags)
            {
                l.Add(f.content);
            }

            return l;
        }

        public List<narrative> GetNarratives(int from = 0, int to = 0, bool viewable = false, bool flagged = false, bool uncategorized = false)
        {
            List<IFilter> filters = new List<IFilter>();

            if (viewable)
            {
                filters.Add(new CategoryFilter(4, false));
                //filters.Add(new FlaggedFilter(false, 1));
                filters.Add(new UserViewableFilter(true));
            }

            if (flagged)
            {
                filters.Add(new FlaggedFilter(true, 0));
            }

            if (uncategorized)
            {
                filters.Add(new CategoryFilter(4, true));
            }

            return NarrativeDAO.GetFilteredList(from, to, filters).ToList();
        }

        public List<Media> GetPlayList(int id)
        {
            return NarrativeDAO.RetrieveNarrative(id).GetPlayList();
        }

        public List<Media> GetSearchList(int id)
        {
            narrative nar = NarrativeDAO.RetrieveNarrative(id);
            if(nar.userViewable)
            {
                return nar.GetPlayList();
            }
            return null;
        }

        public narrative Save(narrative n, List<Expression<Func<narrative, object>>> modified = null)
        {
            if (n == null)
            {
                return null;
            }
            if (n.narrativeID <= 0)
            {
                return NarrativeDAO.InsertNarrative(n);
            }
            else
            {
                if (modified == null)
                {
                    throw new ArgumentNullException("Save() requires list of modified properties");
                }
                return NarrativeDAO.UpdateNarrative(n, modified);
            }
        }

        public List<narrative> SortNarratives(List<narrative> l, String sortby)
        {
           Dictionary<string, Func<List<narrative>, List<narrative>>> sorter =
            new Dictionary<string, Func<List<narrative>, List<narrative>>>
            {
                {"MostViews",       a => a.OrderByDescending(n => n.numberViews).ToList()},
                {"MostAgrees",      a => a.OrderByDescending(n => n.numberAgrees).ToList()},
                {"MostNeutrals",    a => a.OrderByDescending(n => n.numberAmbivalent).ToList()},
                {"MostDisagrees",   a => a.OrderByDescending(n => n.numberDisagrees).ToList()},
                {"MostComments",    a => a.OrderByDescending(n => n.comments.Count).ToList()},
                {"DateAdded",       a => a.OrderByDescending(n => (int)n.date.Ticks).ToList()}
            };

            l = sorter[sortby].Invoke(l);
            return l;
        }

        public void UpdateAudio(narrative n)
        {
            NarrativeDAO.UpdateAudio(n);
        }

        public void UpdatePicture(narrative n)
        {
            NarrativeDAO.UpdatePicture(n);
        }

        public int GetTotalViews()
        {
            List<narrative> n = NarrativeDAO.RetrieveAllNarratives();

            int views = 0;
            foreach (var v in n)
	        {
		        views += v.numberViews;
	        }

            return views;
        }

        public int GetTotalComments()
        {
            List<narrative> n = NarrativeDAO.RetrieveAllNarratives();

            int comments = 0;
            foreach (var v in n)
            {
                comments += v.comments.Count();
            }

            return comments;
        }

        public int GetTotalFlagged()
        {
            List<narrative> n = NarrativeDAO.RetrieveAllNarratives();

            int flags = 0;
            foreach (var v in n)
            {
                if (v.narrativeflags.Count() > 0)
                    flags++;
            }

            return flags;
        }

    }
}
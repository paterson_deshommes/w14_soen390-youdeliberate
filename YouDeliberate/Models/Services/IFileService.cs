﻿using System.Web;
using SystemWrapper.IO;

namespace YouDeliberate.Models.Services
{
    public interface IFileService
    {
        /// <summary>
        /// Extract the POSTed ZIP file to narPath.
        /// </summary>
        /// <param name="file">POSTed ZIP file</param>
        /// <param name="directoryPath">Directory where the ZIP was uploaded</param>
        /// <param name="narPath">Directory where the ZIP will be extracted</param>
        /// <returns>The path of the ZIP file on the server</returns>
        string ExtractFile(HttpPostedFileBase file, IDirectoryInfoWrap directoryPath, IDirectoryInfoWrap narPath);

        /// <summary>
        /// Delete Temporary Folder
        /// </summary>
        /// <param name="path">Path To The Folder to Be Deleted</param>
        void Delete(string path);
    }
}

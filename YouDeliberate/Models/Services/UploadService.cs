﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Xml.Serialization;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.DAO;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Globalization;
using SystemWrapper.IO;

namespace YouDeliberate.Models.Services
{
    public class UploadService : IUploadService
    {
        private INarrativeService dataService;

        public UploadService()
        {
            this.dataService = new NarrativeService();
        }

        public UploadService(INarrativeService dataService)
        {
            this.dataService = dataService;
        }

        public void UploadNarratives(IDirectoryInfoWrap directoryPath, IDirectoryInfoWrap narPath, string[] narrFiles)
        {
            XMLModel narrativeXml;
            narrative narrative = new narrative();
            string[] XML_PATTERN = { "*.xml" };

            foreach (string folder in narrFiles)
            {
                var fold = new DirectoryInfoWrap(folder);
                string xmlFile = GetFilesFromDirectory(fold, XML_PATTERN)[0];
                using (StreamReader reader = new StreamReader(xmlFile))
                {
                    string xml = reader.ReadToEnd();
                    narrativeXml = XMLModel.ParseXML(xml);
                }
                ValidateXMLInput(narrativeXml,fold);

				// Insert the narrative into the database and get an ID
                narrative = narrativeXml.XMLtoDB(narPath.ToString());

                // Move the narrative to a new directory according to the ID
                var newPath = new DirectoryInfo(directoryPath + @"\" + narrative.narrativeID.ToString()).ToString();
                Directory.Move(folder, newPath);
                narrative.path = newPath;

                List<Expression<Func<narrative, object>>> modified = new List<Expression<Func<narrative, object>>>();
                modified.Add(n => n.path);

                narrative = dataService.Save(narrative, modified);

                // Add the audio and picture files to the database
                DirectoryInfoWrap narrativePath = new DirectoryInfoWrap(narrative.path);
                AddAudioFiles(narrative, narrativePath);
                AddPictureFiles(narrative, narrativePath);
            }
        }

        public void ValidateXMLInput(XMLModel narr, IDirectoryInfoWrap path)
        {
            if (!((narr.language.ToLower() == "english") || (narr.language.ToLower() == "french")))
            {
                throw new ArgumentException("XML invalid Language - [English] or [French] Only. Inside Narrative Folder "+path.Name);
            }
            
            if(!(Regex.IsMatch(narr.time,"[0-9]{2}-[0-9]{2}-[0-9]{2}")))
            {
                throw new ArgumentException("XML invalid time - Format [##-##-##] Only. Inside Narrative Folder " + path.Name);
            }

            string[] formats = {"yyyy-MM-dd"};
            DateTime parsedDateTime;

            if (!(DateTime.TryParseExact(narr.submitDate, formats, new CultureInfo("en-US"), DateTimeStyles.None, out parsedDateTime)))
            {
                throw new ArgumentException("XML Invalid Date or Format [yyyy-MM-dd] Only. Inside Narrative Folder " + path.Name);
            }
        }

        public void AddAudioFiles(narrative narrative, IDirectoryInfoWrap narrativePath)
        {
            string[] AUDIO_PATTERN = { "*.mp3" };
            string[] audioFiles = GetFilesFromDirectory(narrativePath, AUDIO_PATTERN);

            foreach (string audioFile in audioFiles)
            {
                audio audio = new audio();
                audio.narrative = narrative;
                //audio.path = audioFile;
                int position = audioFile.LastIndexOf('\\');
                string s = "";
                if (position > -1)
                    s = audioFile.Substring(position + 1);
                audio.path = s;

                narrative.audios.Add(audio);
            }

            dataService.UpdateAudio(narrative);
        }

        public void AddPictureFiles(narrative narrative, IDirectoryInfoWrap narrativePath)
        {
            string[] PICTURE_PATTERN = { "*.jpg", "*.jpeg" };
            string[] pictureFiles = GetFilesFromDirectory(narrativePath, PICTURE_PATTERN);

            foreach(string pictureFile in pictureFiles)
            {
                picture picture = new picture();
                picture.narrative = narrative;
                //picture.path = pictureFile;
                int position = pictureFile.LastIndexOf('\\');
                string s = "";
                if (position > -1)
                    s = pictureFile.Substring(position + 1);
                picture.path = s;

                narrative.pictures.Add(picture);
            }

            dataService.UpdatePicture(narrative);
        }

        public string[] GetFilesFromDirectory(IDirectoryInfoWrap path, string[] extensions)
        {
            List<string> files = new List<string>();
            foreach(string ext in extensions)
            {
                foreach(IFileInfoWrap file in path.GetFiles(ext))
                {
                    files.Add(file.FullName);
                }
            }

            return files.ToArray();
        }
    }
}
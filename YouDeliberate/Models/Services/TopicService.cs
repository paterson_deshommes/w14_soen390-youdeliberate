﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using YouDeliberate.Models.DAO;
using YouDeliberate.Models.Entity;

namespace YouDeliberate.Models.Services
{
    public class TopicService : ITopicService
    {
        private ITopicDAO topDAO;

        public TopicService()
        {
            topDAO = new TopicDAO();
        }

        public TopicService(ITopicDAO top)
        {
            topDAO = top;
        }

        public List<string> GetCurrentTopic()
        {
            topic tp = new topic();
            tp = topDAO.GetCurTopic();

            return GetTopicList(tp);
        }

        public List<string> GetListAllTopics()
        {
            List<topic> topics = topDAO.GetTopicList();
            List<string> results = GetTopicList(topics);

            return GetTopicList(topics);
        }

        public List<string> GetTopicList(List<topic> topics)
        {
            List<string> results = new List<string>();

            foreach (topic tp in topics)
            {
                results.Add(tp.topicID.ToString());
                results.Add(tp.content);
                results.Add(tp.subcontent);
                results.Add(tp.inUse.ToString());
            }

            return results;
        }
        private List<string> GetTopicList(topic tp)
        {
            List<string> topicContent = new List<string>();
            topicContent.Add(tp.content);
            topicContent.Add(tp.subcontent);
            topicContent.Add(GetTopicFormat("backgroundImage.png", tp.topicID));
            topicContent.Add(GetTopicFormat("introEnglish.mp4", tp.topicID));
            topicContent.Add(GetTopicFormat("introFrench.mp4", tp.topicID));
            return topicContent;
        }

        private string GetTopicFormat(string fileName,int topicId)
        {
            string topicFormat = "../../Topics/" + topicId + "/" + fileName;
            return topicFormat;
        }

        public void AddTopic(HttpPostedFile bgImg, HttpPostedFile enVid, HttpPostedFile frVid, topic topic)
        {
            topic newTopic = topDAO.InsertTopic(topic);
            if (newTopic != null)
            {
                string path = CreateFolder(newTopic);
                AddTopicImage(bgImg, path);
                AddTopicVids(enVid, frVid, path);
            }
            else
            {
                throw new System.ArgumentException("Parameter topic cannot be null");
            }
        }

        private void AddTopicImage(HttpPostedFile filetoUpload,string path)
        {
            string fullPathWithFileName = path + "backgroundImage.png";
            string FilePath = HttpContext.Current.Server.MapPath(fullPathWithFileName);
            filetoUpload.SaveAs(FilePath);
        }

        private void AddTopicVids(HttpPostedFile enVid, HttpPostedFile frVid,string path)
        {
            string fullPathWithFileName = path + "introEnglish.mp4";
            string FilePath = HttpContext.Current.Server.MapPath(fullPathWithFileName);
            enVid.SaveAs(FilePath);
            fullPathWithFileName = path + "introFrench.mp4";
            FilePath = HttpContext.Current.Server.MapPath(fullPathWithFileName);
            frVid.SaveAs(FilePath);
        }

        private string CreateFolder(topic tp)
        {
            string topID = tp.topicID.ToString();
            string path = "~/Topics/" + topID + "/";

            bool isExists = System.IO.Directory.Exists(HttpContext.Current.Server.MapPath(path));
            if (!isExists)
                System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath(path));
            return path;
        }

        public topic ChangeOnlineTopic(int id)
        {
            topic current = topDAO.GetCurTopic();
            List<Expression<Func<topic, object>>> attr = SetTopicInUse(current, false);
            topDAO.UpdateTopic(current, attr);
            topic newTopic = topDAO.GetTopic(id);
            attr = SetTopicInUse(newTopic, true);
            topDAO.UpdateTopic(newTopic, attr);
            return newTopic;
        }

        private List<Expression<Func<topic, object>>> SetTopicInUse(topic tp, bool flag)
        {
            List<Expression<Func<topic, object>>> attr = new List<Expression<Func<topic, object>>>();
            tp.inUse = flag;
            attr.Add(top => top.inUse);
            return attr;
        }
      
    }
}
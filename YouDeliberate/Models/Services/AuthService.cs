﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using YouDeliberate.Models.Entity;
using YouDeliberate.Models.DAO;

namespace YouDeliberate.Models.Services
{
    public class AuthService : IAuthService
    {
        
        public string VerifyLogin(String view,HttpSessionStateBase Session)
        {
            string viewReturn = null;

            if (Session["admin"] != null)
            {
                viewReturn = view;
            }
            else
            {
                viewReturn = "Index";
            }

            return viewReturn;
        }

        public void Logout(HttpSessionStateBase Session)
        {
            Session.Abandon();
            Session.Remove("admin");
            Session.Clear();
        }

        public Boolean Login(String userLogin,String passLogin,HttpSessionStateBase Session)
        {

            var user = userLogin;
            var password = passLogin;
            EncryptService encrypt = new EncryptService();

            string newPass = encrypt.Encrypt(password);

            administrator newAdmin = new administrator();
            AdminDAO adDAO = new AdminDAO();

            newAdmin = adDAO.RetrieveAdmin(user, newPass);

            if (newAdmin != null)
            {
                Session["admin"] = newAdmin.username;
                return true;
            }

            return false;
        }
    }
}
﻿using System.Web;
using System.Web.Optimization;

namespace YouDeliberate
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
            "~/Content/base.css",
            "~/Content/layout.css",
            "~/Content/skeleton.css",
            "~/Content/custom.css",
            "~/Content/font-awesome/css/font-awesome.css",
            "~/Content/fancybox/jquery.fancybox.css",
            "~/Content/jplayer.css"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/analytics").Include(
            "~/Scripts/_analytics.js"));

            bundles.Add(new ScriptBundle("~/bundles/csspie").Include(
            "~/Scripts/CSSpie.js"));

            bundles.Add(new ScriptBundle("~/bundles/lemmon").Include(
            "~/Scripts/lemmon-slider.js"));

            bundles.Add(new ScriptBundle("~/bundles/fancybox").Include(
            "~/Scripts/jquery.fancybox.js"));

            bundles.Add(new ScriptBundle("~/bundles/jplayer").Include(
            "~/Scripts/jQuery.jPlayer.2.5.0/jquery.jplayer.js",
            "~/Scripts/jQuery.jPlayer.2.5.0/add-on/jplayer.playlist.js",
            "~/Scripts/jQuery.jPlayer.2.5.0/add-on/jquery.jplayer.inspector.js"));
        }
    }
}